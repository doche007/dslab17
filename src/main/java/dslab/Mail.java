package dslab;

public class Mail {

    /**
     * Variables for Constructor
     */
    private String[] toUsers = null;
    private String fromUser = null;
    private String subject = " no subject";
    private String data = " no data";
    private String hash = null;
    private Boolean isUserNotified = false;
    private int messageID;


    /**
     * Default Constructor
     */
    public Mail() {
    }

    /**
     * Constructor
     * @param toUsers - String that contains emails from users where the message should be delivered
     * @param fromUser - String that contains information who sends the message
     * @param subject - String that describe the subject of a message
     * @param data - String that contains the message
     * @param messageID - Unique message ID
     */
    public Mail(String[] toUsers, String fromUser, String subject, String data, String hash, int messageID) {
        this.toUsers = toUsers;
        this.fromUser = fromUser;
        this.subject = subject;
        this.data = data;
        this.hash = hash;
        this.messageID = messageID;
    }

    /**
     * Constructor
     * @param toUsers - String that contains emails from users where the message should be delivered
     * @param fromUser - String that contains information who sends the message
     * @param subject - String that describe the subject of a message
     * @param data - String that contains the message
     */
    public Mail(String[] toUsers, String fromUser, String subject, String data, String hash) {
        this.toUsers = toUsers.clone();
        this.fromUser = fromUser;
        this.subject = subject;
        this.data = data;
        this.hash = hash;
    }


    public String[] getToUsers() {
        return toUsers;
    }

    public void setToUsers(String[] toUsers) {
        this.toUsers = toUsers.clone();
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Boolean getUserNotified() {
        return isUserNotified;
    }

    public void setUserNotified(Boolean userNotified) {
        isUserNotified = userNotified;
    }

    public int getMessageID() {
        return messageID;
    }

    public void setMessageID(int messageID) {
        this.messageID = messageID;
    }

}
