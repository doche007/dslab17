package dslab.nameserver;

import java.io.InputStream;
import java.io.PrintStream;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import dslab.ComponentFactory;
import dslab.Shell.NameserverShell;
import dslab.util.Config;

public class Nameserver implements INameserver, INameserverRemote {

    private final ConcurrentSkipListMap<String, INameserverRemote> childRemoteObjects = new ConcurrentSkipListMap<>(String::compareToIgnoreCase);
    private final ConcurrentSkipListMap<String, String> mailboxAddresses = new ConcurrentSkipListMap<>(String::compareToIgnoreCase);
    private Registry registry;
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    private String root_id;
    private String registry_host;
    private int registry_port;
    private String domain;
    private InputStream in;
    private PrintStream out;
    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public Nameserver(String componentId, Config config, InputStream in, PrintStream out) {
        this.in = in;
        this.out = out;
        this.root_id = config.getString("root_id");
        this.registry_host = config.getString("registry.host");
        this.registry_port = config.getInt("registry.port");
        if (config.containsKey("domain")){
            this.domain = config.getString("domain");
        }
        LOGGER.setLevel(Level.INFO);

    }

    @Override
    public void registerNameserver(String domain, INameserverRemote nameserver) throws RemoteException, AlreadyRegisteredException, InvalidDomainException {
        String[] splitDomain = domain.split("\\.");
        if (splitDomain.length > 1){
            String lastDomain = splitDomain[splitDomain.length - 1];
            synchronized (childRemoteObjects) {
                if (childRemoteObjects.containsKey(lastDomain)) {
                    StringBuilder newDomain = new StringBuilder(splitDomain[0]);
                    for (int i = 1; i < splitDomain.length - 1; i++) {
                        newDomain.append(".");
                        newDomain.append(splitDomain[i]);
                    }
                    childRemoteObjects.get(lastDomain).registerNameserver(newDomain.toString(), nameserver);
                } else {
                    throw new InvalidDomainException("Invalid domain!");
                }
            }
        }else {
            synchronized (childRemoteObjects) {
                if (childRemoteObjects.containsKey(domain)) {
                    throw new AlreadyRegisteredException("Nameserver has already been registered!");
                } else {
                    childRemoteObjects.put(domain, nameserver);
                    out.println(format(new LogRecord(Level.INFO, "Registering nameserver for zone '" + domain + "'")));
                    out.flush();
                }
            }
        }
    }

    @Override
    public void registerMailboxServer(String domain, String address) throws RemoteException, AlreadyRegisteredException, InvalidDomainException {
        String[] splitDomain = domain.split("\\.");
        if (splitDomain.length > 1) {
            String lastDomain = splitDomain[splitDomain.length - 1];
            synchronized (childRemoteObjects) {
                if (childRemoteObjects.containsKey(lastDomain)) {
                    StringBuilder newDomain = new StringBuilder(splitDomain[0]);
                    for (int i = 1; i < splitDomain.length - 1; i++) {
                        newDomain.append(".");
                        newDomain.append(splitDomain[i]);
                    }
                    childRemoteObjects.get(lastDomain).registerMailboxServer(newDomain.toString(), address);
                } else {
                    throw new InvalidDomainException("Invalid domain!");
                }
            }
        }else {
            synchronized (mailboxAddresses) {
                if (mailboxAddresses.containsKey(domain)) {
                    throw new AlreadyRegisteredException("Nameserver has already been registered!");
                } else {
                    mailboxAddresses.put(domain, address);
                    out.println(format(new LogRecord(Level.INFO, "Registering mailboxserver '" + domain + "'")));
                    out.flush();
                }
            }
        }
    }

    @Override
    public INameserverRemote getNameserver(String zone) throws RemoteException {
        return childRemoteObjects.getOrDefault(zone, null);
    }

    @Override
    public String lookup(String name) throws RemoteException {
        out.println(format(new LogRecord(Level.INFO, "Nameserver for '" + name + "' requested by transfer server")));
        out.flush();
        return mailboxAddresses.getOrDefault(name, null);
    }


    @Override
    public void removeRemoteObject(String domain) throws RemoteException{
        String[] splitDomain = domain.split("\\.");
        if (splitDomain.length > 1) {
            String lastDomain = splitDomain[splitDomain.length - 1];
            StringBuilder newDomain = new StringBuilder(splitDomain[0]);
            for (int i = 1; i < splitDomain.length - 1; i++){
                newDomain.append(".");
                newDomain.append(splitDomain[i]);
            }
            synchronized (childRemoteObjects) {
                childRemoteObjects.get(lastDomain).removeRemoteObject(newDomain.toString());
            }
        }else {
            synchronized (childRemoteObjects) {
                childRemoteObjects.remove(domain);
                out.println(format(new LogRecord(Level.INFO, "Removing nameserver for zone '" + domain + "'")));
                out.flush();
            }
        }
    }


    @Override
    public void removeMailboxServer(String domain) throws RemoteException{
        String[] splitDomain = domain.split("\\.");
        if (splitDomain.length > 1) {
            String lastDomain = splitDomain[splitDomain.length - 1];
            StringBuilder newDomain = new StringBuilder(splitDomain[0]);
            for (int i = 1; i < splitDomain.length - 1; i++){
                newDomain.append(".");
                newDomain.append(splitDomain[i]);
            }
            synchronized (childRemoteObjects) {
                childRemoteObjects.get(lastDomain).removeMailboxServer(newDomain.toString());
            }
        }else {
            synchronized (mailboxAddresses) {
                mailboxAddresses.remove(domain);
                out.println(format(new LogRecord(Level.INFO, "Removing mailboxserver '" + domain + "'")));
                out.flush();
            }
        }
    }

    @Override
    public void run() {
        try {
            INameserverRemote remoteObject;
            if (domain == null){
                //Root Nameserver
                registry = LocateRegistry.createRegistry(registry_port);
                remoteObject = (INameserverRemote) UnicastRemoteObject.exportObject(this, 0);
                registry.bind(root_id, remoteObject);
            }else {
                remoteObject = (INameserverRemote) UnicastRemoteObject.exportObject(this, 0);
                //Zone Nameserver
                ((INameserverRemote)LocateRegistry.getRegistry(registry_host, registry_port).lookup(root_id)).registerNameserver(domain, remoteObject);
            }

            /*
             * Variable for shell and shell execution
            */
            NameserverShell shell = new NameserverShell(this.in, this.out, this);
            shell.run();

            shutdown();

        } catch (RemoteException | NotBoundException e) {
            // out.println("Error Nameserver");
            shutdown();
        } catch (AlreadyRegisteredException | InvalidDomainException e){
            out.println(e.getMessage());
            shutdown();
        } catch (AlreadyBoundException e){
            out.println("Error AlreadyBoundException");
            shutdown();
        }

    }

    /**
     * Prints out each known nameserver (zones) in alphabetical order,
     * from the perspective of this nameserver
     */
    @Override
    public void nameservers() {
        if (childRemoteObjects.isEmpty()){
            out.println("there are no nameservers registered yet");
            out.flush();
        }else {
            int counter = 1;
            for (String key : childRemoteObjects.keySet()) {
                out.println(counter + ". " + key);
                out.flush();
                counter++;
            }
        }
    }

    /**
     * Prints out some information about each stored mailbox server address, containing mail domain and
     * address (IP:port), arranged by the domain in alphabetical order.
     */
    @Override
    public void addresses() {
        if (mailboxAddresses.isEmpty()){
            out.println("there are no mailboxservers registered yet");
            out.flush();
        }else {
            int counter = 1;
            StringBuilder stringBuilder;
            for (String key : mailboxAddresses.keySet()) {
                stringBuilder = new StringBuilder();
                stringBuilder.append(counter);
                stringBuilder.append(". ");
                stringBuilder.append(key);
                stringBuilder.append(" ");
                stringBuilder.append(mailboxAddresses.get(key));
                out.println(stringBuilder);
                out.flush();
                counter++;
            }
        }
    }

    /**
     * Performs a shutdown of a server
     */
    @Override
    public void shutdown() {
        try {
            UnicastRemoteObject.unexportObject(this,true);
        } catch (NoSuchObjectException e) {
            out.println("Error while unexporting object: " + e.getMessage());
            out.flush();
        }

        if (domain == null){
            try {
                registry.unbind(root_id);
                UnicastRemoteObject.unexportObject(registry,true);
            } catch (Exception e) {
                out.println("Error while unbinding object: " + e.getMessage());
                out.flush();
            }
        }else {
            /*
             * Removing this RemoteObject from parents map
             */
            try {
                ((INameserverRemote)LocateRegistry.getRegistry(registry_host, registry_port).lookup(root_id)).removeRemoteObject(domain);
            } catch (Exception e) {
                out.println("Error while removing RemoteObject from parents map: " + e.getMessage());
                out.flush();
            }
        }
    }

    /**
     * Method for calculating of time(date).
     *
     * @param ms - milliseconds
     * @return Date string in format HH:mm:ss
     */
    private String calcDate(long ms){
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(ms);
        return dateFormat.format(date);
    }

    /**
     *
     * @param rec - LogRecord with message
     * @return String containing date(HH:mm:ss) + " : " + Log Message
     */
    private String format(LogRecord rec){
        return calcDate(rec.getMillis()) + " : " + rec.getMessage();
    }

    public static void main(String[] args) throws Exception {
        INameserver component = ComponentFactory.createNameserver(args[0], System.in, System.out);
        component.run();
    }

}
