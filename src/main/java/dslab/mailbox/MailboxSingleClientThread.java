package dslab.mailbox;

import dslab.EncryptedStreams.EncryptedInStream;
import dslab.EncryptedStreams.EncryptedOutStream;
import dslab.Mail;
import dslab.util.Config;
import dslab.util.Keys;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class MailboxSingleClientThread implements Runnable {

    /**
     * Variables for constructor.
     */
    private String componentId;
    private ServerSocket serverSocket;
    private ConcurrentHashMap<String, List<Mail>> contentForClients;
    private Socket clientSocket;
    private Config users;

    /**
     * Constructor for MailboxSingleClientThread which is responsible for communication with clients.
     * @param serverSocket - The socket created in main server, used to check if it is still working.
     * @param contentForClients - The map where all mails are stored.
     * @param clientSocket - Socket, which is used for connection with client.
     * @param users - Configuration that contains users with their passwords
     */
    MailboxSingleClientThread(String componentId, ServerSocket serverSocket, ConcurrentHashMap<String, List<Mail>> contentForClients, Socket clientSocket, Config users) {
        this.componentId = componentId;
        this.serverSocket = serverSocket;
        this.contentForClients = contentForClients;
        this.clientSocket = clientSocket;
        this.users = users;
    }

    private boolean secureConnectionStarted = false;

    private EncryptedOutStream encryptedOutStream;
    private EncryptedInStream encryptedInStream;

    @Override
    public void run() {
        /*
         * loggedInFlag - used to check if user is logged in.
         * loggedInAs - used to store the name of user that is logged in.
         * startsecure - used to check if secured connection is already established
         */
        boolean logIn = false;
        String loggedInAs = null;
        try {
            encryptedOutStream = new EncryptedOutStream(new PrintWriter(clientSocket.getOutputStream()));
            encryptedInStream = new EncryptedInStream(new BufferedReader(new InputStreamReader(clientSocket.getInputStream())));

            encryptedOutStream.print("ok DMAP2.0", secureConnectionStarted);

            String clientMessage;

            while (!clientSocket.isClosed() && (clientMessage = encryptedInStream.readLine(secureConnectionStarted)) != null && !serverSocket.isClosed()){
                //Client message is split into individual words /[space] or [tab]
                String[] clientMessageWords = clientMessage.split("\\s+");

                if (clientMessageWords[0].equals("login")) {
                    if (logIn && loggedInAs != null){
                        encryptedOutStream.print("error you are already logged in", secureConnectionStarted);
                    }else if (clientMessageWords.length < 3){
                        encryptedOutStream.print("error not enough arguments for log in", secureConnectionStarted);
                    }else {
                        String userName = clientMessageWords[1];
                        String password = clientMessageWords[2];
                        if (users.containsKey(userName)){
                            if (users.getString(userName).equals(password)){
                                logIn = true;
                                loggedInAs = userName;
                                encryptedOutStream.print("ok", secureConnectionStarted);
                            }else {
                                encryptedOutStream.print("error wrong password", secureConnectionStarted);
                            }
                        }else {
                            encryptedOutStream.print("error unknown user", secureConnectionStarted);
                        }
                    }
                }else if (clientMessageWords[0].equals("quit")) {
                    logIn = false;
                    loggedInAs = null;
                    encryptedOutStream.print("ok bye", secureConnectionStarted);
                    encryptedInStream.close();
                    encryptedOutStream.close();
                    clientSocket.close();
                } else if (clientMessageWords[0].equals("startsecure")) {
                    if (secureConnectionStarted) {
                        encryptedOutStream.print("The secure connection is already established", secureConnectionStarted);
                    } else {
                        //Server sending S (plain) : ok <component-id> to client (MSG 2)
                        encryptedOutStream.print("ok " + componentId, secureConnectionStarted);
                        secureConnectionStarted = true;
                        //Server reading his Private RSA Key
                        PrivateKey serverRSAPrivateKey = Keys.readPrivateKey(new File("keys/server/" + componentId));
                        try {
                            //Setting the Cipher for encrypting the message
                            String RSA = "RSA/NONE/OAEPWithSHA256AndMGF1Padding";
                            encryptedInStream.setCipher(RSA, serverRSAPrivateKey);
                        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException e) {
                            break;
                        }
                        //Receiving new message from client (MSG 3)
                        clientMessage = encryptedInStream.readLine(secureConnectionStarted);
                        String[] splitClientMessage = clientMessage.split("\\s");
                        if (splitClientMessage[0].equals("ok") && splitClientMessage.length == 4) {
                            //Client Challenge (random number) Base64.encoded
                            String clientChallenge = splitClientMessage[1];
                            //Creating secret AES key that client has sent
                            SecretKeySpec secretKey = new SecretKeySpec(Base64.decode(splitClientMessage[2]), "AES");
                            //IvParameter that is sent by client
                            IvParameterSpec ivParameter = new IvParameterSpec(Base64.decode(splitClientMessage[3]));

                            //Sending response to client (MSG 4)
                            try {
                                String AES = "AES/CTR/NoPadding";
                                encryptedOutStream.setCipher(AES, secretKey, ivParameter);
                                encryptedInStream.setCipher(AES, secretKey, ivParameter);
                                encryptedOutStream.print("ok " + clientChallenge, secureConnectionStarted);


                                //Receiving response from client (MSG 5)
                                clientMessage = encryptedInStream.readLine(secureConnectionStarted);
                                if (!clientMessage.equals("ok")) {
                                    break;
                                }
                            } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException e) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }else {
                    if (loggedInAs != null) {
                        List<Mail> mailList = contentForClients.get(loggedInAs);
                        if (clientMessageWords[0].equals("list")) {
                                StringBuilder message;
                                synchronized (mailList) {
                                    for (Mail mail : mailList) {
                                        message = new StringBuilder();
                                        message.append(mail.getMessageID());
                                        message.append(" ");
                                        message.append(mail.getFromUser());
                                        message.append(mail.getSubject());

                                        encryptedOutStream.print(message.toString(), secureConnectionStarted);
                                    }
                                }
                                encryptedOutStream.print("ok", secureConnectionStarted);
                        } else if (clientMessageWords[0].equals("show")) {
                                if (clientMessageWords.length < 2) {
                                    encryptedOutStream.print("error not enough arguments for show", secureConnectionStarted);
                                } else {
                                    int messageID;
                                    boolean isMessageShown = false;
                                    try {
                                        messageID = Integer.parseInt(clientMessageWords[1]);
                                        synchronized (mailList) {
                                            for (Mail mail : mailList) {
                                                if (mail.getMessageID() == messageID) {
                                                    showMail(mail, encryptedOutStream);
                                                    isMessageShown = true;
                                                    break;
                                                }
                                            }
                                        }
                                    } catch (NumberFormatException e) {
                                        //Ignored
                                    }
                                    if (!isMessageShown) {
                                        encryptedOutStream.print("error unknown message id", secureConnectionStarted);
                                    }
                                }
                        } else if (clientMessageWords[0].equals("delete")) {
                                if (clientMessageWords.length < 2) {
                                    encryptedOutStream.print("error not enough arguments for show", secureConnectionStarted);
                                } else {
                                    int messageID;
                                    boolean isMessageDeleted = false;
                                    try {
                                        messageID = Integer.parseInt(clientMessageWords[1]);
                                        synchronized (mailList) {
                                            for (Mail mail : mailList) {
                                                if (mail.getMessageID() == messageID) {
                                                    mailList.remove(mail);
                                                    isMessageDeleted = true;
                                                    encryptedOutStream.print("ok", secureConnectionStarted);
                                                    break;
                                                }
                                            }
                                        }
                                    } catch (NumberFormatException e) {
                                        //Ignored
                                    }
                                    if (!isMessageDeleted) {
                                        encryptedOutStream.print("error unknown message id", secureConnectionStarted);
                                    }
                                }
                        } else if (clientMessageWords[0].equals("logout")) {
                                logIn = false;
                                loggedInAs = null;
                                encryptedOutStream.print("ok", secureConnectionStarted);
                        }
                        //When user inserts a unknown command connection is closed
                        else {
                            encryptedOutStream.print("error unknown command", secureConnectionStarted);
                            encryptedInStream.close();
                            encryptedOutStream.close();
                            clientSocket.close();
                        }
                    }else {
                        encryptedOutStream.print("error not logged in", secureConnectionStarted);
                    }
                }
            }

        }catch (IOException | BadPaddingException | IllegalBlockSizeException e){
            // Ignored because we cannot handle it
        }finally {
            try {
                encryptedOutStream.close();
                encryptedInStream.close();
                if (!clientSocket.isClosed()){
                    clientSocket.close();
                }
            } catch (IOException e) {
                // Ignored because we cannot handle it
            }
        }
    }

    /**
     * Method used to show the Mail to user.
     * @param mail - Mail that is going to bee shown.
     * @param encryptedOutStream - output stream to send the message.
     */
    private void showMail(Mail mail, EncryptedOutStream encryptedOutStream) throws IOException, BadPaddingException, IllegalBlockSizeException {
        StringBuilder mailContent = new StringBuilder();

        mailContent.append("from ");
        mailContent.append(mail.getFromUser());
        encryptedOutStream.print(mailContent.toString(), secureConnectionStarted);

        mailContent = new StringBuilder();
        mailContent.append("to ");
        mailContent.append(mail.getToUsers()[0]);
        for (int i = 1; i < mail.getToUsers().length; i++){
            mailContent.append(",");
            mailContent.append(mail.getToUsers()[i]);
        }
        encryptedOutStream.print(mailContent.toString(), secureConnectionStarted);

        mailContent = new StringBuilder();
        mailContent.append("subject");
        mailContent.append(mail.getSubject());
        encryptedOutStream.print(mailContent.toString(), secureConnectionStarted);

        mailContent = new StringBuilder();
        mailContent.append("data");
        mailContent.append(mail.getData());
        encryptedOutStream.print(mailContent.toString(), secureConnectionStarted);

        mailContent = new StringBuilder();
        mailContent.append("hash ");
        if (mail.getHash() != null) {
            mailContent.append(mail.getHash());
        }
        encryptedOutStream.print(mailContent.toString(), secureConnectionStarted);

        encryptedOutStream.print("ok", secureConnectionStarted);
    }
}
