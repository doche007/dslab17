package dslab.mailbox;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dslab.ComponentFactory;
import dslab.Mail;
import dslab.Shell.TransferAndMailboxShell;
import dslab.nameserver.AlreadyRegisteredException;
import dslab.nameserver.INameserverRemote;
import dslab.nameserver.InvalidDomainException;
import dslab.nameserver.Nameserver;
import dslab.util.Config;

public class MailboxServer implements IMailboxServer, Runnable {

    /**
     * Variables for mailbox server socket and executor service (threads)
     */
    private ServerSocket listenerForTransfer;
    private ServerSocket listenerForClient;
    private ExecutorService fixedThreadPool;

    /**
     * Variables for Nameserver lookup
     */
    private String domain;
    private String root_id;
    private String registry_host;
    private int registry_port;

    /**
     * Variables for constructor
     */
    private String componentId;
    private Config config;
    private InputStream in;
    private PrintStream out;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MailboxServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.componentId = componentId;
        this.config = config;
        this.in = in;
        this.out = out;
        domain = config.getString("domain");
        root_id = config.getString("root_id");
        registry_host = config.getString("registry.host");
        registry_port = config.getInt("registry.port");
    }

    @Override
    public void run() {

        try {
            ConcurrentHashMap<String, List<Mail>> contentForClients = new ConcurrentHashMap<>();

            //Opening config that contains users with their passwords to insert them into map.
            Config users = new Config(config.getString("users.config"));
            for (String user : users.listKeys()) {
                contentForClients.put(user, Collections.synchronizedList(new LinkedList<>()));
            }

            /*
             * Creating a fixed thread pool size = 2;
             * One for server which listen to user connections
             * One for server which listen to transfer server connections
             */
            fixedThreadPool = Executors.newFixedThreadPool(2);

            /*
             * Opening sockets for 2 listener servers (for client and transfer server)
             */
            listenerForClient = new ServerSocket(config.getInt("dmap.tcp.port"));
            listenerForTransfer = new ServerSocket(config.getInt("dmep.tcp.port"));

            /*
             * Creating and executing thread for listening server for clients
             */

            fixedThreadPool.execute(new MailboxListeningServer(contentForClients, componentId, listenerForClient, true, domain, users));

            /*
             * Creating and executing thread for listening server for transfer servers
             */
            fixedThreadPool.execute(new MailboxListeningServer(contentForClients, componentId, listenerForTransfer, false, domain, users));

            /*
             * Registration of Mailbox Domain by Root server
             */
            try {
                ((INameserverRemote) LocateRegistry.getRegistry(registry_host, registry_port).lookup(root_id)).registerMailboxServer(domain,(new Config("domains")).getString(domain));
            } catch (AlreadyRegisteredException | InvalidDomainException | NotBoundException | IOException e) {
                out.println("Error by registration of MailboxServer");
            }

            /*
             * Opening shell to block the main
             */
            TransferAndMailboxShell shell = new TransferAndMailboxShell(this.in, this.out);
            shell.run();

            shutdown();
        } catch (IOException e) {
            fixedThreadPool.shutdownNow();
        }


    }

    @Override
    public void shutdown() {

        try {
            try {
                ((INameserverRemote) LocateRegistry.getRegistry(registry_host, registry_port).lookup(root_id)).removeMailboxServer(domain);
            } catch (NullPointerException | NotBoundException | IOException e) {
                out.println("Error by removing Mailboxserver from zone Nameserver");
            }
            if (!listenerForClient.isClosed()) {
                listenerForClient.close();
            }
            if (!listenerForTransfer.isClosed()) {
                listenerForTransfer.close();
            }

            fixedThreadPool.shutdownNow();

        }catch (IOException e){
            fixedThreadPool.shutdownNow();
        }

    }

    public static void main(String[] args) throws Exception {
        IMailboxServer server = ComponentFactory.createMailboxServer(args[0], System.in, System.out);
        server.run();
    }
}
