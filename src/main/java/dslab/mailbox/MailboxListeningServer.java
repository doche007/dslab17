package dslab.mailbox;

import dslab.Mail;
import dslab.util.Config;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MailboxListeningServer implements Runnable {

    /**
     * Variables for constructor.
     */
    private ConcurrentHashMap<String, List<Mail>> contentForClients;
    private String componentId;
    private ServerSocket serverSocket;
    private boolean isForClients;
    private String domain;
    private Config users;

    /**
     * Constructor for MailboxListeningServer (for client or transfer), and this Server
     * is going to run new thread for every client/transfer server that is going to connect with it.
     * @param contentForClients - The map where all mails are stored.
     * @param serverSocket - The socket created in main server, used to check if it is still working.
     * @param isForClients - Boolean variable used to determine if this listening server is for clients or transfer servers
     * @param domain - The domain of Mailbox Server
     * @param users - Configuration that contains users with their passwords
     */
    MailboxListeningServer(ConcurrentHashMap<String, List<Mail>> contentForClients, String componentId, ServerSocket serverSocket, boolean isForClients, String domain, Config users) {
        this.contentForClients = contentForClients;
        this.componentId = componentId;
        this.serverSocket = serverSocket;
        this.isForClients = isForClients;
        this.domain = domain;
        this.users = users;
    }

    @Override
    public void run() {

        Socket socket;
        ExecutorService threadPool = Executors.newCachedThreadPool();

        //List of sockets(connections), which is used to force shut down the connection.
//        Queue<Socket> listOfSockets = new LinkedList<>();

        //This part is implementation for Clients ListeningServer
        while (!serverSocket.isClosed()) {
            try {
                socket = serverSocket.accept();
                if (isForClients) {
                    threadPool.execute(new MailboxSingleClientThread(componentId, serverSocket, contentForClients, socket, users));
                }else {
                    threadPool.execute(new MailboxSingleTransferThread(contentForClients, socket, domain));
                }
                //The new socket is added to list of sockets.
//                listOfSockets.add(socket);

//                for (Socket a : listOfSockets){
//                    if (a.isClosed()){
//                        listOfSockets.remove(a);
//                    }
//                }
            } catch (IOException e) {
                threadPool.shutdownNow();
            }
        }
        threadPool.shutdownNow();
    }
}

