package dslab.mailbox;

import dslab.Mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class MailboxSingleTransferThread implements Runnable {

    /**
     * Variables for constructor.
     */
    private ConcurrentHashMap<String, List<Mail>> contentForClients;
    private Socket transferServerSocket;
    private String domain;

    /**
     * Constructor for MailboxSingleTransferThread which is responsible for communication with transfer servers.
     * @param contentForClients - The map where all mails are stored.
     * @param transferServerSocket - Socket, which is used for connection with transfer server.
     * @param domain - The domain of Mailbox Server
     */
    MailboxSingleTransferThread(ConcurrentHashMap<String, List<Mail>> contentForClients, Socket transferServerSocket, String domain) {
        this.contentForClients = contentForClients;
        this.transferServerSocket = transferServerSocket;
        this.domain = domain;
    }

    @Override
    public void run() {
        Mail mail = new Mail();
        try {
            //Opening printWriter and bufferedReader for message exchange between Mailbox and TransferServer
            PrintWriter printWriter = new PrintWriter(transferServerSocket.getOutputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(transferServerSocket.getInputStream()));

            printWriter.println("ok DMEP2.0");
            printWriter.flush();

            String receivedMessage;
            StringBuilder stringBuilder;

            while (!transferServerSocket.isClosed() && (receivedMessage = bufferedReader.readLine()) != null){
                String[] receivedMessageWords = receivedMessage.split("\\s+");
                if (receivedMessageWords[0].equals("begin")){
                    printWriter.println("ok");
                    printWriter.flush();
                }else if (receivedMessageWords[0].equals("from")) {
                    mail.setFromUser(receivedMessageWords[1]);
                    printWriter.println("ok");
                    printWriter.flush();
                }else if (receivedMessageWords[0].equals("to")){
                    if (receivedMessageWords.length < 2){
                        printWriter.println("error no argument (recipient)");
                        printWriter.flush();
                    }else {
                        String[] recipients = receivedMessageWords[1].split(",");
                        mail.setToUsers(recipients);
                        int numberOfValidRecipients = checkIfAllUsersAreValid(recipients);
                        if (numberOfValidRecipients > 0){
                            printWriter.println("ok " + numberOfValidRecipients);
                            printWriter.flush();
                        } else {
                            printWriter.println("error unknown recipient");
                            printWriter.flush();
                        }
                    }
                }else if (receivedMessageWords[0].equals("subject")) {
                    stringBuilder = new StringBuilder();
                    for (int i = 1; i < receivedMessageWords.length; i++) {
                        stringBuilder.append(" ");
                        stringBuilder.append(receivedMessageWords[i]);
                    }
                    mail.setSubject(stringBuilder.toString());
                    printWriter.println("ok");
                    printWriter.flush();
                }else if (receivedMessageWords[0].equals("data")) {
                    stringBuilder = new StringBuilder();
                    for (int i = 1; i < receivedMessageWords.length; i++) {
                        stringBuilder.append(" ");
                        stringBuilder.append(receivedMessageWords[i]);
                    }
                    mail.setData(stringBuilder.toString());
                    printWriter.println("ok");
                    printWriter.flush();
                }else if (receivedMessageWords[0].equals("hash")){
                    mail.setHash(receivedMessageWords[1]);
                    printWriter.println("ok");
                    printWriter.flush();
                }else if (receivedMessageWords[0].equals("send")) {
                    for (String recipient : mail.getToUsers()){
                        int indexOfAt = recipient.indexOf("@");
                        if (indexOfAt != -1) {
                            String name = recipient.substring(0, indexOfAt);
                            String domain = recipient.substring(indexOfAt + 1);
                            if (domain.equals(this.domain)) {
                                if (contentForClients.containsKey(name)) {
                                    synchronized (contentForClients.get(name)) {
                                        if (contentForClients.get(name).size() > 0) {
                                            mail.setMessageID(contentForClients.get(name).get(contentForClients.get(name).size() - 1).getMessageID() + 1);
                                        } else {
                                            mail.setMessageID(1);
                                        }
                                    }
                                    contentForClients.get(name).add(new Mail(mail.getToUsers(), mail.getFromUser(), mail.getSubject(), mail.getData(), mail.getHash(), mail.getMessageID()));
                                }
                            }
                        }
                    }
                    printWriter.println("ok");
                    printWriter.flush();
                }else if (receivedMessageWords[0].equals("quit")){
                    printWriter.println("ok bye");
                    printWriter.flush();
                    transferServerSocket.close();
                }else {
                    printWriter.println("error protocol error");
                    printWriter.flush();
                    transferServerSocket.close();
                }
            }
            bufferedReader.close();
            printWriter.close();
            if (!transferServerSocket.isClosed()) {
                transferServerSocket.close();
            }
        } catch (IOException e) {
            // Ignored because we cannot handle it
        }
    }

    /**
     * Method that is used to return the number of valid recipients (-1 when there is at least one invalid)
     * @param recipients - all recipients, where the mail is going to be delivered
     * @return - returns -1 when at least one recipient with this domain is invalid, else returns number of valid recipients
     */
    private int checkIfAllUsersAreValid(String[] recipients){
        int counter = 0;
        for (String recipient : recipients){
            int indexOfAt = recipient.indexOf("@");
            if (indexOfAt != -1) {
                String name = recipient.substring(0, indexOfAt);
                String domain = recipient.substring(indexOfAt + 1);
                if (domain.equals(this.domain)) {
                    if (!contentForClients.containsKey(name)) {
                        return -1;
                    }
                    counter++;
                }
            }
        }
        return counter;
    }
}
