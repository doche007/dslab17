package dslab.client;

import java.io.*;
import java.net.Socket;
import java.security.*;
import java.util.LinkedList;
import java.util.Queue;

import dslab.ComponentFactory;
import dslab.EncryptedStreams.EncryptedInStream;
import dslab.EncryptedStreams.EncryptedOutStream;
import dslab.Shell.ClientShell;
import dslab.util.Config;
import dslab.util.Keys;
import org.bouncycastle.util.encoders.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;

public class MessageClient implements IMessageClient, Runnable {

    private Socket dmapSocket;
    private EncryptedInStream dmapEncryptedInStream;
    private EncryptedOutStream dmapEncryptedOutStream;
    private boolean secureConnectionStarted = false;

    /**
     * Variables for constructor
     */
    private String componentId;
    private Config config;
    private InputStream in;
    private PrintStream out;

    /**
     * Creates a new client instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MessageClient(String componentId, Config config, InputStream in, PrintStream out) {
        this.componentId = componentId;
        this.config = config;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {
        try{
            //creating a tcp socket at specified host and port
            dmapSocket = new Socket(config.getString("mailbox.host"), config.getInt("mailbox.port"));

            //create a reader to receive message from the server
            dmapEncryptedInStream = new EncryptedInStream(new BufferedReader(new InputStreamReader(dmapSocket.getInputStream())));

            //create a writer to send messages to mailbox server
            dmapEncryptedOutStream = new EncryptedOutStream(new PrintWriter(dmapSocket.getOutputStream()));


            if (isSecureConnectionEstablished()){
                //User login on Mailbox Server
                dmapEncryptedOutStream.print("login " + config.getString("mailbox.user") + " " + config.getString("mailbox.password"), secureConnectionStarted);

                if (dmapEncryptedInStream.readLine(secureConnectionStarted).equals("ok")){
                    /*
                     * Variable for shell and shell execution
                     */
                    ClientShell shell = new ClientShell(this.in, this.out, this);
                    shell.run();
                }
            }
        } catch (IOException | BadPaddingException | IllegalBlockSizeException e) {
            // Ignored because we cannot handle it
        }finally {
            shutdown();
        }
    }

    private boolean isSecureConnectionEstablished(){
        try {
            String responseString;
            responseString = dmapEncryptedInStream.readLine(secureConnectionStarted);
            if (!responseString.startsWith("ok DMAP2.0")){
                return false;
            }
            //Sending MSG 1 C (plain): "startsecure"
            dmapEncryptedOutStream.print("startsecure", secureConnectionStarted);
            //Receiving MSG 2 S (plain) : "ok" <component-id>
            responseString = dmapEncryptedInStream.readLine(secureConnectionStarted);
            if (responseString == null){
                return false;
            }
            if (responseString.startsWith("ok")){
                String[] responseWords = responseString.split("\\s+");

                PublicKey serverRSAPublicKey = Keys.readPublicKey(new File("keys/client/" + responseWords[1] + ".pub"));
                dmapEncryptedOutStream.setCipher("RSA/NONE/OAEPWithSHA256AndMGF1Padding", serverRSAPublicKey);
                secureConnectionStarted = true;

                //Sending MSG 3 C (RSA): ok <client-challenge> <secret-key> <iv>
                //Generating 32byte random number
                SecureRandom secureRandom = new SecureRandom();
                final byte[] number = new byte[32];
                secureRandom.nextBytes(number);
                String clientChallenge = new String(Base64.encode(number));

                //Generating an AES secret key
                KeyGenerator generator = KeyGenerator.getInstance("AES");
                generator.init(256);
                SecretKey secretKey = generator.generateKey();
                String encodedKey = new String(Base64.encode(secretKey.getEncoded()));

                //Generating IV parameter
                byte[] ivBytes = new byte[16];
                secureRandom.nextBytes(ivBytes);
                IvParameterSpec ivParameter = new IvParameterSpec(ivBytes);
                String ivString = new String(Base64.encode(ivParameter.getIV()));

                dmapEncryptedOutStream.print("ok " + clientChallenge + " " + encodedKey + " " + ivString, secureConnectionStarted);

                //Receiving MSG 4 S (AES): ok <client-challenge>
                dmapEncryptedInStream.setCipher("AES/CTR/NoPadding", secretKey, ivParameter);
                dmapEncryptedOutStream.setCipher("AES/CTR/NoPadding", secretKey, ivParameter);
                responseString = dmapEncryptedInStream.readLine(secureConnectionStarted);
                if (responseString == null){
                    return false;
                }
                if (responseString.startsWith("ok")){
                    responseWords = responseString.split("\\s+");
                    if (!clientChallenge.equals(responseWords[1])){
                        return false;
                    }
                    //Sending MSG 5 C (AES): ok
                    dmapEncryptedOutStream.print("ok", secureConnectionStarted);
                }else {
                    return false;
                }
            }else {
                return false;
            }
        } catch (BadPaddingException | IllegalBlockSizeException | IOException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String generateHashCode(String msg) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        Key hashKey = Keys.readSecretKey(new File("keys/hmac.key"));
        Mac hMac = Mac.getInstance("HmacSHA256");
        hMac.init(hashKey);

        byte[] hash = hMac.doFinal(msg.getBytes());
        return (new String(Base64.encode(hash)));
    }

    @Override
    public void inbox() {
        try {
            dmapEncryptedOutStream.print("list", secureConnectionStarted);
            String receivedMsg;
            Queue<String> idList = new LinkedList<>();
            while (!(receivedMsg = dmapEncryptedInStream.readLine(secureConnectionStarted)).equals("ok")){
                String[] msgID = receivedMsg.split("\\s+");
                idList.add(msgID[0]);
            }
            if (idList.isEmpty()){
                out.println("Your inbox is empty");
                out.flush();
            }else {
                while (!idList.isEmpty()) {
                    String id = idList.poll();
                    dmapEncryptedOutStream.print("show " + id, secureConnectionStarted);
                    out.println("Mail " + id);
                    out.flush();
                    while (!(receivedMsg = dmapEncryptedInStream.readLine(secureConnectionStarted)).equals("ok")) {
                        out.println(receivedMsg);
                        out.flush();
                    }
                    out.println();
                    out.flush();
                }
            }
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            out.println("error some error occurred while printing inbox");
            out.flush();
        } catch (IOException e){
            out.println("error connection is broken");
            out.flush();
        }
    }

    @Override
    public void delete(String id) {
        try {
            dmapEncryptedOutStream.print("delete " + id, secureConnectionStarted);
            String response = dmapEncryptedInStream.readLine(secureConnectionStarted);
            out.println(response);
            out.flush();
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            out.println("error by sending/receiving message to/from the mailbox server");
            out.flush();
        } catch (IOException e){
            out.println("error connection is broken");
            out.flush();
        }
    }

    @Override
    public void verify(String id) {
        try {
            dmapEncryptedOutStream.print("show " + id,secureConnectionStarted);
            String hash = "";
            String receivedMessage;
            String from = "";
            String subject = "";
            String to = "";
            String data = "";
            while (!(receivedMessage = dmapEncryptedInStream.readLine(secureConnectionStarted)).equals("ok")){
                if (receivedMessage.startsWith("from")){
                    from = receivedMessage.substring(5);
                }else if (receivedMessage.startsWith("to")) {
                    to = receivedMessage.substring(3);
                }else if (receivedMessage.startsWith("subject")){
                    subject = receivedMessage.substring(8);
                }else if(receivedMessage.startsWith("data")){
                    data = receivedMessage.substring(5);
                }else if (receivedMessage.startsWith("hash")){
                    hash = receivedMessage.substring(5);
                }else {
                    out.println(receivedMessage);
                    out.flush();
                    break;
                }
            }
            if (receivedMessage.equals("ok")) {
                String msg = String.join("\n", from, to, subject, data);
                if (hash.equals(generateHashCode(msg))) {
                    out.println("ok");
                    out.flush();
                } else {
                    out.println("error hash not equals");
                    out.flush();
                }
            }
        } catch (BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidKeyException e) {
            out.println("error by verifying message");
            out.flush();
        } catch (IOException e){
            out.println("error connection is broken");
            out.flush();
        }

    }

    @Override
    public void msg(String to, String subject, String data) {
        try {
            String from = config.getString("transfer.email");

            String msg = String.join("\n", from, to, subject, data);

            String hashCode = generateHashCode(msg);

            Socket dmepSocket = new Socket(config.getString("transfer.host"), config.getInt("transfer.port"));

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(dmepSocket.getInputStream()));
            PrintWriter printWriter = new PrintWriter(dmepSocket.getOutputStream());

            String receivedMessage;

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok DMEP2.0")){
                    printWriter.println("begin");
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok")){
                    printWriter.println("from " + from);
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok")) {
                    printWriter.println("to " + to);
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.startsWith("ok")) {
                    printWriter.println("subject " + subject);
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok")) {
                    printWriter.println("data " + data);
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok")) {
                    printWriter.println("hash " + hashCode);
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok")) {
                    printWriter.println("send");
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null) {
                if (receivedMessage.equals("ok")){
                    out.println("ok");
                    out.flush();
                }
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException | IOException e) {
            out.println("error by msg sending");
            out.flush();
        }
    }

    @Override
    public void shutdown() {
        try {
            if (!dmapSocket.isClosed()) {
                try {
                    dmapSocket.close();
                } catch (IOException e) {
                    // Ignored because we cannot handle it
                }
            }
            dmapEncryptedOutStream.close();
            try {
                dmapEncryptedInStream.close();
            } catch (IOException e) {
                // Ignored because we cannot handle it
            }
        }catch (NullPointerException e){
            out.println("error mailbox server is not available");
            out.flush();
        }
    }

    public static void main(String[] args) throws Exception {
        IMessageClient client = ComponentFactory.createMessageClient(args[0], System.in, System.out);
        client.run();
    }
}
