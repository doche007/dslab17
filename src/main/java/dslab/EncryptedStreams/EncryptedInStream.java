package dslab.EncryptedStreams;

import org.bouncycastle.util.encoders.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.BufferedReader;
import java.io.IOException;

public class EncryptedInStream extends EncryptedStream{

    private BufferedReader plainTextBufferedReader;

    public EncryptedInStream(BufferedReader plainTextBufferedReader) {
        this.plainTextBufferedReader = plainTextBufferedReader;
    }


    /**
     * * Method used to decrypt message and to return it as String
     *
     * @param securedConnectionStarted - Boolean used to check if the message should be decrypted.
     * @return - Returns decrypted message as String
     * @throws IOException - Exception produced by failed or interrupted I/O operation.
     * @throws BadPaddingException - This exception is thrown when a particular padding mechanism is expected for the input data but the data is not padded properly.
     * @throws IllegalBlockSizeException - This exception is thrown when the length of data provided to a block cipher is incorrect, i.e., does not match the block size of the cipher.
     */
    public String readLine(boolean securedConnectionStarted) throws IOException, BadPaddingException, IllegalBlockSizeException {
        String message = plainTextBufferedReader.readLine();
        if (securedConnectionStarted) {
            String decryptedMessage;
            if (message != null) {
                byte[] data = getCipher().doFinal(Base64.decode(message.getBytes()));
                decryptedMessage = new String(data);
            }else {
                throw new IOException();
            }
            return decryptedMessage;
        }else {
            return message;
        }
    }

    /**
     * Method used to close plainTextBufferedReader
     *
     * @throws IOException - Exception produced by failed or interrupted I/O operation.
     */
    public void close() throws IOException {
        plainTextBufferedReader.close();
    }

    /**
     * Override method from abstract class EncryptedStream, which returns DECRYPT_MODE
     *
     * @return - DECRYPT_MODE
     */
    @Override
    int mode() {
        return Cipher.DECRYPT_MODE;
    }
}
