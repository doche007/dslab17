package dslab.EncryptedStreams;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Abstract class used to implement common methods for EncryptedOutStream and EncryptedInStream
 */
public abstract class EncryptedStream {

    private Cipher cipher;

    Cipher getCipher() {
        return cipher;
    }

    public void setCipher(String algorithm, Key k) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        this.cipher = Cipher.getInstance(algorithm);
        this.cipher.init(mode(), k);
    }

    public void setCipher(String algorithm, Key k, IvParameterSpec ivParameterSpec) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        this.cipher = Cipher.getInstance(algorithm);
        cipher.init(mode(), k, ivParameterSpec);
    }

    /**
     * Abstract method that will return corresponding mode
     *
     * @return ENCRYPT_MODE for EncryptedOutStream and DECRYPT_MODE for EncryptedInStream
     */
    abstract int mode();
}
