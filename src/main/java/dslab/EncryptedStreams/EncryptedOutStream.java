package dslab.EncryptedStreams;

import org.bouncycastle.util.encoders.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.IOException;
import java.io.PrintWriter;

public class EncryptedOutStream extends EncryptedStream{

    private PrintWriter plainTextPrintWriter;


    public EncryptedOutStream(PrintWriter plainTextPrintWriter) {
        this.plainTextPrintWriter = plainTextPrintWriter;
    }

    /**
     * Method used to print message (encrypted or plain text).
     *
     * @param plainMessage - Message that should print encrypted (not as plain text)
     * @param securedConnectionStarted - Boolean used to check if the message needs to be encrypted before print.
     * @throws BadPaddingException - This exception is thrown when a particular padding mechanism is expected for the input data but the data is not padded properly.
     * @throws IllegalBlockSizeException - This exception is thrown when the length of data provided to a block cipher is incorrect, i.e., does not match the block size of the cipher.
     */
    public void print(String plainMessage, boolean securedConnectionStarted) throws IOException, BadPaddingException, IllegalBlockSizeException {
        if (securedConnectionStarted){
            byte[] data = getCipher().doFinal(plainMessage.getBytes());
            String encryptedMessage = new String(Base64.encode(data));
            plainTextPrintWriter.println(encryptedMessage);
            plainTextPrintWriter.flush();
        }else {
            plainTextPrintWriter.println(plainMessage);
            plainTextPrintWriter.flush();
        }
    }

    /**
     * Method used to close plainTextPrintWriter
     */
    public void close(){
        plainTextPrintWriter.close();
    }

    /**
     * Override method from abstract class EncryptedStream, which returns ENCRYPT_MODE
     *
     * @return - ENCRYPT_MODE
     */
    @Override
    int mode() {
        return Cipher.ENCRYPT_MODE;
    }


}
