package dslab.transfer;

import dslab.Mail;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ListeningServer implements Runnable {

    /**
     * variables for constructor
     */
    private ServerSocket serverSocket;
    private BlockingQueue<Mail> mails;

    /**
     * Constructor for ListeningServer who is going to run new thread
     * for every client that is going to connect with it.
     * @param serverSocket - The socket created in main server, used to check if it is still working.
     * @param mails - A Queue used to store mails, which need to be forwarded to Mailbox Server.
     */
    ListeningServer(ServerSocket serverSocket, BlockingQueue<Mail> mails) {
        this.serverSocket = serverSocket;
        this.mails = mails;
    }

    /**
     * An ExecutorService cachedThreadPool is created,
     * which is used to execute new threads for connections with clients.
     * While the main server socket is active, the listening server is accepting
     * new connections from client side.
     * For each client is new thread executed (object from SingleClientThread class).
     */
    @Override
    public void run() {
        Socket clientSocket;
        ExecutorService clientsThreadPool = Executors.newCachedThreadPool();

        //List of clients, which is used to force shut down the client connection.
        Queue<Socket> clientListOfSockets = new LinkedList<>();

        while (!serverSocket.isClosed()){
            try {
                clientSocket = serverSocket.accept();

                clientsThreadPool.execute(new SingleClientThread(serverSocket, mails, clientSocket));

                //The new client is added to list of clients.
                clientListOfSockets.add(clientSocket);

                for (Socket a : clientListOfSockets){
                    if (a.isClosed()){
                        clientListOfSockets.remove(a);
                    }
                }
            }
            catch (SocketException e) {
                //The main server is closed and so the all opened client connections are going to be closed.
                for (Socket a : clientListOfSockets){
                    try {
                        if (!a.isClosed()) {
                            a.close();
                        }
                    } catch (IOException e1) {
                        // Ignored because we cannot handle it
                    }
                }
                clientsThreadPool.shutdownNow();
            } catch (IOException e) {
                clientsThreadPool.shutdownNow();
            }
        }
        clientsThreadPool.shutdownNow();
    }
}
