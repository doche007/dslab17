package dslab.transfer;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import dslab.ComponentFactory;
import dslab.Mail;
import dslab.Shell.TransferAndMailboxShell;
import dslab.util.Config;

public class TransferServer implements ITransferServer, Runnable {

    /**
     * Variables for constructor
     */
    private String componentId;
    private Config config;
    private InputStream in;
    private PrintStream out;

    /**
     * Variables for server socket and executor service (threads)
     */
    private ServerSocket mainServerSocket;
    private ExecutorService fixedThreadPool;

    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public TransferServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.componentId = componentId;
        this.config = config;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {

        try {
            /*
             * Creating a queue to store mails ready for forwarding
             * which will be forwarded by ForwardServer to MailboxServer
             */
            BlockingQueue<Mail> mailsToForward = new LinkedBlockingQueue<>();

            /*
             * Creating a fixed thread pool size = 2;
             * One for server which listen to user connections
             * One for server which will forward messages to Mailbox server
             */
            fixedThreadPool = Executors.newFixedThreadPool(2);

            /*
             * Opening the main server socket
             */
            mainServerSocket = new ServerSocket(config.getInt("tcp.port"));

            /*
             * Creating and executing thread for listening server
             */
            fixedThreadPool.execute(new ListeningServer(mainServerSocket, mailsToForward));

            /*
             * Creating and executing thread for delivery server
             */
            fixedThreadPool.execute(new DeliveryServer(mainServerSocket, mailsToForward, config));

            /*
             * Variable for shell and shell execution
             */
            TransferAndMailboxShell shell = new TransferAndMailboxShell(this.in, this.out);
            shell.run();

            shutdown();
        } catch (IOException e) {
            fixedThreadPool.shutdownNow();
        }


    }

    @Override
    public void shutdown() {
        try {
            if (!mainServerSocket.isClosed()){
                mainServerSocket.close();

            }
            fixedThreadPool.shutdownNow();
        } catch (IOException e) {
            fixedThreadPool.shutdownNow();
        }
    }

    public static void main(String[] args) throws Exception {
        ITransferServer server = ComponentFactory.createTransferServer(args[0], System.in, System.out);
        server.run();
    }

}
