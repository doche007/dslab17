package dslab.transfer;

import dslab.Mail;
import dslab.nameserver.INameserverRemote;
import dslab.util.Config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;

public class DeliveryServer implements Runnable{

    /**
     * Variables for Nameserver lookup
     */
    private String root_id;
    private String registry_host;
    private int registry_port;

    private ServerSocket serverSocket;
    private BlockingQueue<Mail> mails;
    private Config configOfServer;

    /**
     * Constructor for DeliveryServer who is going to deliver messages to MailboxServer
     * @param serverSocket - The socket created in main server, used to check if it is still working.
     * @param mails - A Queue used to store mails, which need to be forwarded to Mailbox Server.
     * @param configOfServer - Used to read servers IP address
     */
    DeliveryServer(ServerSocket serverSocket, BlockingQueue<Mail> mails, Config configOfServer) {
        this.serverSocket = serverSocket;
        this.mails = mails;
        this.configOfServer = configOfServer;
        root_id = configOfServer.getString("root_id");
        registry_host = configOfServer.getString("registry.host");
        registry_port = configOfServer.getInt("registry.port");
    }

    @Override
    public void run() {
        while (!serverSocket.isClosed() || !mails.isEmpty()){
            if (!mails.isEmpty()){
                Mail mail = mails.poll();

                //List of a domains that have already been visited
                Queue<String> visitedDomains = new LinkedList<>();
                for (String user : mail.getToUsers()){
                    int indexOfAt = user.indexOf('@');
                    if (indexOfAt != -1) {
                        String domainFromUser = user.substring(indexOfAt + 1);

                        String[] splitDomain = domainFromUser.split("\\.");
                        try {
                            INameserverRemote remoteObject = ((INameserverRemote) LocateRegistry.getRegistry(registry_host, registry_port).lookup(root_id));
                            for (int i = splitDomain.length - 1; i > 0 && remoteObject != null; i--){
                                remoteObject = remoteObject.getNameserver(splitDomain[i]);
                            }
                            if (remoteObject != null) {
                                String hostPortCombined = remoteObject.lookup(splitDomain[0]);
                                if (hostPortCombined != null && !visitedDomains.contains(domainFromUser)) {
                                    String host = hostPortCombined.substring(0, hostPortCombined.indexOf(":"));
                                    String port = hostPortCombined.substring(hostPortCombined.indexOf(":") + 1);
                                    visitedDomains.add(domainFromUser);
                                    sendDataToMonitorServer(mail);
                                    forwardTheMailToMailboxServer(mail, host, port);
                                }else {
                                    //Checking if the mail is not sent by server, notify the user
                                    //Else do nothing = the mail will be discarded
                                    if (!mail.getUserNotified() && !mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                                        notifySenderAboutBadRecipients(mail);
                                        mail.setUserNotified(true);
                                    }
                                }
                            }else {
                                //Checking if the mail is not sent by server, notify the user
                                //Else do nothing = the mail will be discarded
                                if (!mail.getUserNotified() && !mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                                    notifySenderAboutBadRecipients(mail);
                                    mail.setUserNotified(true);
                                }
                            }
                        } catch (RemoteException | NotBoundException e) {
                            if (!mail.getUserNotified() && !mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                                notifySenderAboutBadRecipients(mail);
                                mail.setUserNotified(true);
                            }
                        }
                    }else {

                        //Checking if the mail is not sent by server, notify the user
                        //Else do nothing = the mail will be discarded
                        if (!mail.getUserNotified() && !mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                            notifySenderAboutBadRecipients(mail);
                            mail.setUserNotified(true);
                        }
                    }
                }
            }
        }
    }


    /**
     * The method that is adding a mail into queue(list), which will be sent to user who entered the wrong domain address for recipient(s)
     * @param mail - Mail that had bad recipient(s) and the sender is going to be notified about it
     */
    private void notifySenderAboutBadRecipients (Mail mail){
        String[] deliverTo = new String[1];
        deliverTo[0] = mail.getFromUser();
        mails.add(new Mail(deliverTo, "mailer@["+ configOfServer.getString("monitoring.host") + "]", " error by sending" + mail.getSubject(), " error one or more recipients are not valid", null));

    }

    /**
     * forwardTheMailToMailBox - is used to forward the mail to Mailbox
     * @param mail - Mail that is going to be sent.
     * @param host - host from Mailbox
     * @param port - port from Mailbox
     */
    private void forwardTheMailToMailboxServer(Mail mail, String host, String port){
        Socket socket;
        BufferedReader bufferedReader;

        try {
            // creating a tcp socket at specified host and port
            socket = new Socket(host, Integer.parseInt(port));

            //create a reader to retrieve messages from the server
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            //create a writer to send messages to server
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream());

            StringBuilder messageToSend;
            String receivedMessage;

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok DMEP2.0")){
                    printWriter.println("begin");
                    printWriter.flush();
                }
            }

            if ((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok")){
                    messageToSend = new StringBuilder();
                    messageToSend.append("to ");
                    messageToSend.append(mail.getToUsers()[0]);
                    for (int i = 1; i < mail.getToUsers().length; i++){
                        messageToSend.append(",");
                        messageToSend.append(mail.getToUsers()[i]);
                    }
                    printWriter.println(messageToSend);
                    printWriter.flush();
                }
            }

            boolean isErrorOccurred = false;
            if((receivedMessage = bufferedReader.readLine()) != null){
                String checker = receivedMessage.substring(0,2);
                if (!checker.equals("ok")){
                    isErrorOccurred = true;
                    if (!mail.getUserNotified() && !mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                        notifySenderAboutBadRecipients(mail);
                        mail.setUserNotified(true);
                    }
                }else {
                    messageToSend = new StringBuilder();
                    messageToSend.append("from ");
                    messageToSend.append(mail.getFromUser());
                    printWriter.println(messageToSend);
                    printWriter.flush();
                }
            }

            if (!isErrorOccurred) {
                if ((receivedMessage = bufferedReader.readLine()) != null) {
                    if (receivedMessage.equals("ok")) {
                        messageToSend = new StringBuilder();
                        messageToSend.append("subject");
                        messageToSend.append(mail.getSubject());
                        printWriter.println(messageToSend);
                        printWriter.flush();
                    }
                }

                if ((receivedMessage = bufferedReader.readLine()) != null) {
                    if (receivedMessage.equals("ok")) {
                        messageToSend = new StringBuilder();
                        messageToSend.append("data");
                        messageToSend.append(mail.getData());
                        printWriter.println(messageToSend);
                        printWriter.flush();
                    }
                }

                if (mail.getHash() != null) {
                    if ((receivedMessage = bufferedReader.readLine()) != null) {
                        if (receivedMessage.equals("ok")) {
                            messageToSend = new StringBuilder();
                            messageToSend.append("hash ");
                            messageToSend.append(mail.getHash());
                            printWriter.println(messageToSend);
                            printWriter.flush();
                        }
                    }
                }

                if ((receivedMessage = bufferedReader.readLine()) != null) {
                    if (receivedMessage.equals("ok")) {
                        messageToSend = new StringBuilder();
                        messageToSend.append("send");
                        printWriter.println(messageToSend);
                        printWriter.flush();
                    }
                }

                if ((receivedMessage = bufferedReader.readLine()) != null) {
                    if (receivedMessage.equals("ok")) {
                        messageToSend = new StringBuilder();
                        messageToSend.append("quit");
                        printWriter.println(messageToSend);
                        printWriter.flush();
                    }
                }
            }else {
                messageToSend = new StringBuilder();
                messageToSend.append("quit");
                printWriter.println(messageToSend);
                printWriter.flush();
            }

            if((receivedMessage = bufferedReader.readLine()) != null){
                if (receivedMessage.equals("ok bye")){
                    bufferedReader.close();
                    printWriter.close();
                    socket.close();
                }
            }
        } catch (IOException e) {
            if (!mail.getUserNotified() && !mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                notifySenderAboutBadRecipients(mail);
                mail.setUserNotified(true);
            }
        }
    }

    private void sendDataToMonitorServer(Mail mail){
        try {
            //Sending a UDP packet to Monitoring Server
            if (!mail.getFromUser().equals("mailer@[" + configOfServer.getString("monitoring.host") + "]")) {
                //building the message for Monitoring Server
                String message = configOfServer.getString("monitoring.host") +
                        ":" +
                        configOfServer.getString("tcp.port") +
                        " " +
                        mail.getFromUser();

                // creating a new UDP socket and packet that is going to be sent to Monitoring Server
                DatagramSocket datagramSocket = new DatagramSocket();
                byte[] buffer = message.getBytes();
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length, InetAddress.getByName(configOfServer.getString("monitoring.host")), configOfServer.getInt("monitoring.port"));
                // send a packet to Monitoring Server
                datagramSocket.send(packet);
                //closing the socket
                datagramSocket.close();
            }
        }catch (IOException e){
            //ignore
        }
    }
}
