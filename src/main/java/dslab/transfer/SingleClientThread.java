package dslab.transfer;

import dslab.Mail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class SingleClientThread implements Runnable {

    /**
     * Variables for constructor.
     */
    private ServerSocket serverSocket;
    private BlockingQueue<Mail> mails;
    private Socket clientSocket;

    /**
     * Constructor for SingleClientThread which is responsible for communication with clients.
     * @param serverSocket - The socket created in main server, used to check if it is still working.
     * @param mails - A Queue used to store mails, which need to be forwarded to Mailbox Server.
     * @param clientSocket - Socket, which is used for connection with client
     */
    SingleClientThread(ServerSocket serverSocket, BlockingQueue<Mail> mails, Socket clientSocket) {
        this.serverSocket = serverSocket;
        this.mails = mails;
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {

        /*
         * beginFlag - used to check if begin has already been entered
         * senderFlag - used to check if the sender is given
         * recipientsFlag - used to check if users are given
         * mail - used to store all the data
         */
        boolean beginFlag = false;
        boolean senderFlag = false;
        boolean recipientsFlag = false;
        Mail mail = new Mail();
        try {
            //Opening printWriter and bufferedReader for message exchange between Client and Server
            PrintWriter printWriter = new PrintWriter(clientSocket.getOutputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            printWriter.println("ok DMEP2.0");
            printWriter.flush();
            String clientMessage;

            while (!clientSocket.isClosed() && (clientMessage = bufferedReader.readLine()) != null && !serverSocket.isClosed()) {
                //Client message is split into individual words /[space] or [tab]
                String[] clientMessageWords = clientMessage.split("\\s+");
                //A long way of checking all possible commands that client can enter
                if (clientMessageWords[0].equals("begin")) {
                    if (beginFlag) {
                        printWriter.println("you already wrote begin once");
                        printWriter.flush();
                    } else {
                        printWriter.println("ok");
                        printWriter.flush();
                        beginFlag = true;
                    }
                } else if (clientMessageWords[0].equals("to")) {
                    if (beginFlag) {
                        if (clientMessageWords.length < 2) {
                            printWriter.println("error no argument (recipient)");
                            printWriter.flush();
                        } else {
                            String[] recipients = clientMessageWords[1].split(",");
                            mail.setToUsers(recipients);
                            printWriter.println("ok " + recipients.length);
                            printWriter.flush();
                            recipientsFlag = true;
                        }
                    } else {
                        printWriter.println("you haven't wrote begin yet");
                        printWriter.flush();
                    }
                } else if (clientMessageWords[0].equals("from")) {
                    if (beginFlag) {
                        if (clientMessageWords.length < 2) {
                            printWriter.println("error no argument (sender)");
                            printWriter.flush();
                        } else {
                            mail.setFromUser(clientMessageWords[1]);
                            printWriter.println("ok");
                            printWriter.flush();
                            senderFlag = true;
                        }
                    } else {
                        printWriter.println("you haven't wrote begin yet");
                        printWriter.flush();
                    }
                } else if (clientMessageWords[0].equals("subject")) {
                    if (beginFlag) {
                        StringBuilder subject = new StringBuilder();
                        for (int i = 1; i < clientMessageWords.length; i++) {
                            subject.append(" ");
                            subject.append(clientMessageWords[i]);
                        }
                        mail.setSubject(subject.toString());
                        printWriter.println("ok");
                        printWriter.flush();
                    } else {
                        printWriter.println("you haven't wrote begin yet");
                        printWriter.flush();
                    }
                } else if (clientMessageWords[0].equals("data")) {
                    if (beginFlag) {
                        StringBuilder data = new StringBuilder();
                        for (int i = 1; i < clientMessageWords.length; i++) {
                            data.append(" ");
                            data.append(clientMessageWords[i]);
                        }
                        mail.setData(data.toString());
                        printWriter.println("ok");
                        printWriter.flush();
                    } else {
                        printWriter.println("you haven't wrote begin yet");
                        printWriter.flush();
                    }
                }else if (clientMessageWords[0].equals("hash")){
                    if (beginFlag){
                        mail.setHash(clientMessageWords[1]);
                        printWriter.println("ok");
                        printWriter.flush();
                    }else {
                        printWriter.println("you haven't wrote begin yet");
                        printWriter.flush();
                    }
                } else if (clientMessageWords[0].equals("send")) {
                    if (beginFlag) {
                        if (!recipientsFlag) {
                            printWriter.println("error no recipients");
                            printWriter.flush();
                        }
                        if (!senderFlag) {
                            printWriter.println("error no sender");
                            printWriter.flush();
                        }
                        if (recipientsFlag && senderFlag) {
                            printWriter.println("ok");
                            printWriter.flush();
                            mails.add(new Mail(mail.getToUsers(), mail.getFromUser(), mail.getSubject(), mail.getData(), mail.getHash()));
                        }
                    } else {
                        printWriter.println("you haven't wrote begin yet");
                        printWriter.flush();
                    }
                } else if (clientMessageWords[0].equals("quit")) {
                    printWriter.println("ok bye");
                    printWriter.flush();
                    bufferedReader.close();
                    printWriter.close();
                    clientSocket.close();
                }
                //When user inserts a unknown command connection is closed
                else {
                    printWriter.println("error protocol error");
                    printWriter.flush();
                    bufferedReader.close();
                    printWriter.close();
                    clientSocket.close();
                }
            }
            /* Case when Main Server is closed and Client is still connected to Client Server.
             * The Client will be disconnected and notified by next command input.
             */
            if (serverSocket.isClosed() && !clientSocket.isClosed()){
                printWriter.println("We are sorry, our main server is not working right now, please come later.");
                printWriter.flush();
                bufferedReader.close();
                printWriter.close();
                clientSocket.close();
            }
            if (clientSocket.isClosed()){
                bufferedReader.close();
                printWriter.close();
            }
        }catch (IOException e){
            // Ignored because we cannot handle it
        }
    }
}
