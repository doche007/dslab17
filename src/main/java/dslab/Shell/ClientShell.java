package dslab.Shell;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.client.MessageClient;

import java.io.InputStream;
import java.io.PrintStream;

public class ClientShell implements IClientShell, Runnable{

    private Shell shell;
    private MessageClient messageClient;

    public ClientShell(InputStream inputStream, PrintStream outputStream, MessageClient messageClient){
        this.messageClient = messageClient;
        /*
         * Creating a new Shell instance and provide an InputStream to read from,
         * and an OutputStream to write to.
         */
        shell = new Shell(inputStream, outputStream);

        /*
         * Registering all commands, that Shell should support.
         */
        shell.register(this);
    }

    @Command
    @Override
    public void inbox() {
        this.messageClient.inbox();
    }

    @Command
    @Override
    public void delete(String messageID) {
        this.messageClient.delete(messageID);
    }

    @Command
    @Override
    public void verify(String messageID) {
        this.messageClient.verify(messageID);
    }

    @Command
    @Override
    public void msg(String to, String subject, String data) {
        this.messageClient.msg(to, subject, data);
    }

    @Command
    @Override
    public void shutdown() {
        shell.out().println("Closing shell");
        throw new StopShellException();
    }

    @Override
    public void run() {
        shell.run();
    }
}
