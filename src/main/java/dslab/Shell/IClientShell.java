package dslab.Shell;

public interface IClientShell extends Runnable, ITransferAndMailboxShell{

    /**
     * Outputs an overview of the user’s entire message inbox including ids, sender, recipients, subjects
     * and message data.
     */
    void inbox();

    /**
     * Deletes the message with the given id. Outputs ok if the message was deleted or error <description>
     * if the message could, e.g., not be found.
     * @param messageID - ID of the message that needs to be deleted.
     */
    void delete(String messageID);

    /**
     * Verifies the integrity of the message with the given id by calculating the hash of the message.
     * @param messageID - ID of the message that needs to be verified.
     */
    void verify(String messageID);

    /**
     * Sends a message to the given recipient with the given subject and data.
     * @param to - Recipient of a mail.
     * @param subject - Subject of the message.
     * @param data - Data of the message.
     */
    void msg(String to, String subject, String data);
}
