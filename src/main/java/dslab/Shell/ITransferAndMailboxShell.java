package dslab.Shell;

public interface ITransferAndMailboxShell extends Runnable{

    /**
     * Performs a shutdown of a server
     */
    void shutdown();
}
