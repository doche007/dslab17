package dslab.Shell;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.monitoring.MonitoringServer;

import java.io.InputStream;
import java.io.PrintStream;

public class MonitoringShell implements IMonitoringShell, Runnable {

    private Shell shell;
    private MonitoringServer monitoringServer;

    public MonitoringShell(InputStream inputStream, PrintStream outputStream, MonitoringServer monitoringServer) {

        this.monitoringServer = monitoringServer;
        /*
         * Creating a new Shell instance and provide an InputStream to read from,
         * and an OutputStream to write to.
         */
        shell = new Shell(inputStream, outputStream);

        /*
         * Registering all commands, that Shell should support.
         */
        shell.register(this);

    }

    @Command
    @Override
    public void addresses() {
        monitoringServer.addresses();
    }

    @Command
    @Override
    public void servers() {
        monitoringServer.servers();
    }

    @Command
    @Override
    public void shutdown() {
        shell.out().println("Closing shell on server");
        throw new StopShellException();
    }

    @Override
    public void run() {
        shell.run();
    }
}
