package dslab.Shell;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;

import java.io.InputStream;
import java.io.PrintStream;

public class TransferAndMailboxShell implements ITransferAndMailboxShell, Runnable {

    private Shell shell;


    public TransferAndMailboxShell (InputStream inputStream, PrintStream outputStream){

        /*
         * Creating a new Shell instance and provide an InputStream to read from,
         * and an OutputStream to write to.
         */
        shell = new Shell(inputStream, outputStream);

        /*
         * Registering all commands, that Shell should support.
         */
        shell.register(this);

    }

    @Override
    public void run() {
        shell.run();
    }

    @Override
    @Command
    public void shutdown() {
        shell.out().println("Closing shell on server");
        throw new StopShellException();
    }
}
