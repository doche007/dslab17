package dslab.Shell;

public interface INameserverShell extends Runnable, ITransferAndMailboxShell{

    /**
     * Prints out each known nameserver (zones) in alphabetical order,
     * from the perspective of this nameserver
     */
    void nameservers();

    /**
     * Prints out some information about each stored mailbox server address, containing mail domain and
     * address (IP:port), arranged by the domain in alphabetical order.
     */
    void addresses();
}
