package dslab.Shell;

import at.ac.tuwien.dsg.orvell.Shell;
import at.ac.tuwien.dsg.orvell.StopShellException;
import at.ac.tuwien.dsg.orvell.annotation.Command;
import dslab.nameserver.Nameserver;

import java.io.InputStream;
import java.io.PrintStream;

public class NameserverShell implements INameserverShell, Runnable {

    private Shell shell;
    private Nameserver nameserver;

    public NameserverShell(InputStream inputStream, PrintStream outputStream, Nameserver nameserver) {
        this.nameserver = nameserver;
        /*
         * Creating a new Shell instance and provide an InputStream to read from,
         * and an OutputStream to write to.
         */
        shell = new Shell(inputStream, outputStream);

        /*
         * Registering all commands, that Shell should support.
         */
        shell.register(this);
    }

    @Command
    @Override
    public void nameservers() {
        nameserver.nameservers();
    }

    @Command
    @Override
    public void addresses() {
        nameserver.addresses();
    }

    @Command
    @Override
    public void shutdown() {
        shell.out().println("Closing shell on server");
        throw new StopShellException();
    }

    @Override
    public void run() {
        shell.run();
    }
}
