package dslab.Shell;

public interface IMonitoringShell extends Runnable, ITransferAndMailboxShell {

    /**
     * Lists all addresses and the number of messages they have sent.
     */
    void addresses();

    /**
     * Lists all servers and the number of messages sent over these servers.
     */
    void servers();
}
