package dslab.monitoring;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

import dslab.ComponentFactory;
import dslab.Shell.MonitoringShell;
import dslab.util.Config;

public class MonitoringServer implements IMonitoringServer {

    /**
     * Variables for constructor
     */
    private String componentId;
    private Config config;
    private InputStream in;
    private PrintStream out;

    /**
     * Variable for UDP connection and data structure to save all information
     */
    private DatagramSocket datagramSocket;
    private Map<String, Integer> servers = new HashMap<>();
    private Map<String, Integer> addresses = new HashMap<>();


    /**
     * Creates a new server instance.
     *
     * @param componentId the id of the component that corresponds to the Config resource
     * @param config the component config
     * @param in the input stream to read console input from
     * @param out the output stream to write console output to
     */
    public MonitoringServer(String componentId, Config config, InputStream in, PrintStream out) {
        this.componentId = componentId;
        this.config = config;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {
        try {
            /*
             * Opening UDP server socket
             */
            datagramSocket = new DatagramSocket(config.getInt("udp.port"));

            new MonitoringServerThread(datagramSocket, servers, addresses).start();

        } catch (SocketException e) {
            // Ignored because we cannot handle it
        }

        /*
         * Variable for shell and shell execution
         */
        MonitoringShell shell = new MonitoringShell(this.in, this.out, this);
        shell.run();

        shutdown();
    }

    @Override
    public void addresses() {
         list(addresses);
    }

    @Override
    public void servers() {
        list(servers);
    }

    @Override
    public void shutdown() {
        if (datagramSocket != null){
            datagramSocket.close();
        }
    }

    /**
     * This method lists map that is given as parameter.
     * @param map - Map that is going to be listed (addresses or servers).
     */
    private void list(Map<String,Integer> map){
        StringBuilder stringBuilder;
        for (String key : map.keySet()){
            stringBuilder = new StringBuilder();
            stringBuilder.append(key);
            stringBuilder.append(" ");
            stringBuilder.append(map.get(key));
            out.println(stringBuilder);
            out.flush();
        }
        if (map.isEmpty()){
            out.println("Currently is empty");
            out.flush();
        }
    }

    public static void main(String[] args) throws Exception {
        IMonitoringServer server = ComponentFactory.createMonitoringServer(args[0], System.in, System.out);
        server.run();
    }

}
