package dslab.monitoring;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Map;

public class MonitoringServerThread extends Thread {

    /**
     * Variables for constructor
     */
    private DatagramSocket datagramSocket;
    private Map<String, Integer> servers;
    private Map<String, Integer> addresses;

    /**
     * Constructor
     * @param datagramSocket - Socket used for UDP packet transfer.
     * @param servers - Map that contains information about usage of transfer servers.
     * @param addresses - Map that contains information about how many mails has some user sent.
     */
    MonitoringServerThread(DatagramSocket datagramSocket, Map<String, Integer> servers, Map<String, Integer> addresses){
        this.datagramSocket = datagramSocket;
        this.servers = servers;
        this.addresses = addresses;
    }

    @Override
    public void run() {

        byte[] buffer;
        DatagramPacket packet;
        try {
            while (true) {
                buffer = new byte[128];

                //creating a datagram packet with length = buffer.length
                packet = new DatagramPacket(buffer, buffer.length);

                //waiting for incoming packets from transfer server
                datagramSocket.receive(packet);

                String receivedMessage = new String(packet.getData());
                receivedMessage = receivedMessage.substring(0, packet.getLength());

                String[] receivedMessageWords = receivedMessage.split("\\s+");

                if (servers.containsKey(receivedMessageWords[0])){
                    servers.put(receivedMessageWords[0],servers.get(receivedMessageWords[0]) + 1);
                }else  {
                    servers.put(receivedMessageWords[0], 1);
                }

                if (addresses.containsKey(receivedMessageWords[1])){
                    addresses.put(receivedMessageWords[1], addresses.get(receivedMessageWords[1]) + 1);
                }else {
                    addresses.put(receivedMessageWords[1], 1);
                }

            }
        } catch (SocketException e) {
            // Ignored because we cannot handle it
        }
        catch (IOException e) {
            // Ignored because we cannot handle it
        }
    }
}
